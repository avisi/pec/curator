# Docker Curator

This repository contains an Elasticsearch Curator installation based on a CentOS base image.

[![pipeline status](https://gitlab.com/avisi/pec/curator/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/curator/commits/master)

---

### Tags

| Tag | Notes |
|-----|-------|
| `latest` | Latest build of v3.3.2 |

## Usage

Example docker-compose file:
```yml
version: '3'
services:
  curator:
    image: registry.gitlab.com/avisi/pec/curator
    environment:

```

## Issues

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/curator/issues).
