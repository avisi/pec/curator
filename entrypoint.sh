#!/bin/sh

set -e

exec /usr/local/bin/tini curator "@"
