FROM registry.gitlab.com/avisi/centos:7 AS downloader

USER root

RUN yum install -y wget

# Install tini
ENV TINI_VERSION v0.18.0
RUN wget https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini && chmod +x tini

# ---
FROM registry.gitlab.com/avisi/base/centos:7

COPY --from=downloader /opt/avisi/tini /usr/local/bin/

USER root

RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" && python get-pip.py && rm get-pip.py \
     && pip install elasticsearch-curator

COPY --chown=avisi:avisi entrypoint.sh /usr/local/bin/entrypoint
COPY --chown=avisi:avisi config/ /opt/avisi/curator/

USER avisi

ENTRYPOINT [ "/usr/local/bin/entrypoint" ]
